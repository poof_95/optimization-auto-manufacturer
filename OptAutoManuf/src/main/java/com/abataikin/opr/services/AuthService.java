package com.abataikin.opr.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("authService")
public class AuthService implements UserDetailsService {

    @Autowired
    private AuthUserLookUpService userLookUpService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userLookUpService.findUser(s);
    }
}
