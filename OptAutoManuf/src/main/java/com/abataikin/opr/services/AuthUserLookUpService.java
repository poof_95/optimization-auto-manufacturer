package com.abataikin.opr.services;

import com.abataikin.opr.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthUserLookUpService {
@Autowired
    BCryptPasswordEncoder passwordEncoder;

    User findUser(String username) {
        User found = null;
        switch (username) {
            case "Abataikin": {
                found = new User("Abataikin", passwordEncoder.encode("alex1234"));
            }
                break;
            case "Guest":{
                found = new User("Guest", passwordEncoder.encode("Guest"));
            }break;
        }
        return found;
    }
}
