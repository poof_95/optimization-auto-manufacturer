package com.abataikin.opr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.PropertySource;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

@SpringBootApplication
@PropertySource("classpath:config.properties")
public class OptAutoManufApp {

    private static final TrayIcon trayIcon = new TrayIcon(Toolkit.getDefaultToolkit()
            .getImage(OptAutoManufApp.class.getResource("/images/1.png")));
    private static final SystemTray systemTray = SystemTray.getSystemTray();
    private static final PopupMenu popup = new PopupMenu();
    private static final MenuItem stopItem = new MenuItem("Остановить сервер");
    private static final MenuItem startItem = new MenuItem("Запустить сервер");
    private static final MenuItem exitItem = new MenuItem("Выход");

    private static ConfigurableApplicationContext ctx;
    private static String[] bufArgs;
    private static int exitCode;

    public static void main(String[] args) {
        bufArgs = args;
        ctx = SpringApplication.run(OptAutoManufApp.class,args);
        addTray();
    }

    private static void addTray(){

        if (SystemTray.isSupported()){

            startItem.setEnabled(false);

            popup.add(startItem);
            popup.add(stopItem);
            popup.addSeparator();
            popup.add(exitItem);

            trayIcon.setImageAutoSize(true);
            trayIcon.setPopupMenu(popup);

            startItem.addActionListener(e ->{
                ctx = SpringApplication.run(OptAutoManufApp.class,bufArgs);
                trayIcon.displayMessage("Сервер запущен!", "Для открытия клиента нажмите здесь.", TrayIcon.MessageType.INFO);
                stopItem.setEnabled(true);
                startItem.setEnabled(false);
            });

            stopItem.addActionListener(e ->{
                exitCode = SpringApplication.exit(ctx,() -> 0);
                trayIcon.displayMessage("Сервер остановлен!", "Для запуска выберите \"Запустить сервер\".", TrayIcon.MessageType.INFO);
                stopItem.setEnabled(false);
                startItem.setEnabled(true);
            });

            exitItem.addActionListener(e -> {
                if (!ctx.isRunning())
                    exitCode = SpringApplication.exit(ctx,() -> 0);
                System.exit(exitCode);
            });

            trayIcon.addActionListener( e -> {
                if (Desktop.isDesktopSupported() && !startItem.isEnabled()) {
                    try {
                        if (ctx.isRunning())
                            exitCode = SpringApplication.exit(ctx,() -> 0);
                        ctx = SpringApplication.run(OptAutoManufApp.class,bufArgs);
                        Desktop.getDesktop().browse(new URI("http://localhost:8080/main"));
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    } catch (URISyntaxException e1) {
                        e1.printStackTrace();
                    }
                }
            });
        }

        try {
            systemTray.add(trayIcon);
            trayIcon.displayMessage("Сервер запущен!", "Для открытия клиента нажмите здесь.", TrayIcon.MessageType.INFO);
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }
}
