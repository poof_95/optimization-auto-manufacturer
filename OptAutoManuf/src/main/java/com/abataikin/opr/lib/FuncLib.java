package com.abataikin.opr.lib;

public class FuncLib {

    public static double resFunc(double c1, double c2, double x1, double x2) {
        return c1 * x1 + c2 * x2;
    }

    public static boolean isLimitOne(double v1, double v2, double x1, double x2, double limit) {
        return v1 * x1 + v2 * x2 <= limit;
    }


    public static boolean isLimitTwo(double v1, double v2, double x1, double x2, double limit) {
        return v1 * x1 + v2 * x2 <= limit;
    }

    public static boolean checkValue(double x, double limit) {
        return x <= limit && x > 0;
    }
}
