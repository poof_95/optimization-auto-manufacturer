package com.abataikin.opr.core;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Random;

import static com.abataikin.opr.lib.FuncLib.isLimitOne;
import static com.abataikin.opr.lib.FuncLib.isLimitTwo;
import static com.abataikin.opr.lib.FuncLib.resFunc;
import static com.abataikin.opr.lib.FuncLib.checkValue;

public class GeneticAlgorithm {

    @Setter
    private double c1;
    @Setter
    private double c2;
    @Getter
    private double x1;
    @Getter
    private double x2;
    @Getter
    private double f;
    @Setter
    private double[] limits;
    @Setter
    private double[][] values;
    @Setter
    private double numberOfGen;
    @Setter
    private double numberOfChild;
    @Setter
    private double numberOfParents;
    @Getter
    private ArrayList<ArrayList<Point>> generations;
    private ArrayList<Point> parents;
    private Random random;

    public GeneticAlgorithm() {
        generations = new ArrayList<>();
        parents = new ArrayList<>();
        random = new Random(System.currentTimeMillis());
    }

    public void solve() {
        generations.clear();
        parents.clear();

        for (int i = 0; i < numberOfParents; i++) {
            int startX1;
            int startX2;
            do {
                startX1 = random.nextInt((int) limits[2] + 1);
                startX2 = random.nextInt((int) limits[3] + 1);
                if (isLimitOne(values[0][0], values[0][1], startX1, startX2, limits[0]) && isLimitTwo(values[1][0], values[1][1], startX1, startX2, limits[1])
                        && checkValue(startX1, limits[2]) && checkValue(startX2, limits[3]))
                    break;
            } while (true);
            parents.add(new Point(startX1, startX2));
        }
        parents.sort(((o1, o2) -> Double.compare(o2.getF(), o1.getF())));
        for (int i = 0; i < numberOfGen; i++) {
            ArrayList<Point> child = new ArrayList<>();
            genAlgorithm(child);
            generations.add(child);
        }

        x1 = generations.get(generations.size() - 1).get(0).getX1();
        x2 = generations.get(generations.size() - 1).get(0).getX2();
        f = generations.get(generations.size() - 1).get(0).getF();
    }

    private void genAlgorithm(ArrayList<Point> child) {
        for (int i = 0; i < numberOfChild; i++) {
            child.add(new Point());
            int parentIndex1 = 0;
            int parentIndex2 = 0;
            while (parentIndex1 == parentIndex2) {
                parentIndex1 = random.nextInt(parents.size());
                parentIndex2 = random.nextInt(parents.size());
            }

            do {
                for (int i1 = 0; i1 < 2; i1++) {
                    if (random.nextInt() % 2 != 0) {
                        child.get(i).crossing(parents.get(parentIndex1), i1);
                    } else {
                        child.get(i).crossing(parents.get(parentIndex2), i1);
                    }
                }
                if (random.nextInt(10) < 5)
                    child.get(i).mutate();
                if (isLimitOne(values[0][0], values[0][1], child.get(i).getX1(), child.get(i).getX2(), limits[0])
                        && isLimitTwo(values[1][0], values[1][1], child.get(i).getX1(), child.get(i).getX2(), limits[1])
                        && checkValue(child.get(i).getX1(), limits[2]) && checkValue(child.get(i).getX2(), limits[3]))
                    break;
            } while (true);
            child.get(i).setF(resFunc(c1, c2, child.get(i).getX1(), child.get(i).getX2()));
        }
        child.sort(((o1, o2) -> Double.compare(o2.getF(), o1.getF())));
        parents.clear();
        for (int i = 0; i < numberOfChild; i++) {
            if (!parents.contains(child.get(i))) {
                parents.add(child.get(i));
            }
        }
    }


    public class Point {
        @Getter
        private double x1;
        @Getter
        private double x2;
        @Getter
        @Setter
        private double f;


        Point(double x1, double x2) {
            this.x1 = x1;
            this.x2 = x2;
        }

        Point() {
            this.x1 = 0;
            this.x2 = 0;
        }

        private void mutate() {
            x1 += (-300) + random.nextInt(300);
            x2 += (-300) + random.nextInt(300);
        }

        private void crossing(Point p, int index) {
            if (index == 0) {
                x1 = p.x1;
            } else {
                x2 = p.x2;
            }
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null)
                return false;
            return x1 == ((Point) obj).x1 && x2 == ((Point) obj).x2 && f == ((Point) obj).f;
        }
    }
}
