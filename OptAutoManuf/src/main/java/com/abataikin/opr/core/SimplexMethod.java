package com.abataikin.opr.core;

import lombok.Getter;
import lombok.Setter;

import javax.swing.*;
import java.util.*;

import static com.abataikin.opr.lib.FuncLib.*;

public class SimplexMethod {

    private final int ROW_COUNT = 5;
    private final int COL_COUNT = 3;

    @Setter
    private double c1;
    @Setter
    private double c2;
    @Getter
    private double x1;
    @Getter
    private double x2;
    @Getter
    private double f;
    @Setter
    private double[][] matr;
    @Getter
    private ArrayList<Matrix> list;

    private Matrix matrix;
    private int indexOfLeadingCol;
    private int indexOfLeadingRow;
    private double leadingElem;
    private int columns;
    private boolean isOpt;

    public SimplexMethod() {
        list = new ArrayList<>();
    }

    public void solve() {
        list.clear();

        indexOfLeadingCol = 0;
        indexOfLeadingRow = 0;
        leadingElem = 0;
        isOpt = false;
        matrix = new Matrix(ROW_COUNT - 1, COL_COUNT);

        for (int i = 0; i < ROW_COUNT; i++) {
            matrix.setRow(i, matr[i]);
        }

        findCol(matrix.getFunc());
        findRow(matrix);
        columns = matrix.getCols();
        Matrix modified = new Matrix(ROW_COUNT - 1, COL_COUNT);
        double tmp;
        double[] leadingRow = matrix.getRow(indexOfLeadingRow);
        double[] curRow = new double[columns];

        list.add(matrix);

        while (!isOpt) {
            for (int i = 0; i < columns; i++) {
                tmp = matrix.getE(indexOfLeadingRow, i);
                curRow[i] = tmp / leadingElem;
            }
            modified.setRow(indexOfLeadingRow, curRow);
            for (int i = 0; i < matrix.getRows(); i++) {
                if (i == indexOfLeadingRow) continue;
                for (int j = 0; j < columns; j++) {
                    double el = matrix.getE(i, j);
                    double el2 = leadingRow[j];
                    double el3 = matrix.getE(i, indexOfLeadingCol);
                    tmp = el - (el2 * el3) / leadingElem;
                    curRow[j] = tmp;
                }
                modified.setRow(i, curRow);
            }

            isOpt = Arrays.stream(modified.getFunc())
                    .allMatch(e -> e >= 0);

            matrix = copy(matrix, modified);

            list.add(matrix);

            findCol(matrix.getFunc());
            findRow(matrix);
            leadingRow = matrix.getRow(indexOfLeadingRow);
        }

        if (isOpt) {

            f = matrix.matrix[matrix.matrix.length - 1][0];
            for (int i = 0; i < matrix.matrix.length; i++) {
                for (int j = 0; j < matrix.matrix.length - 1; j++) {
                    if (i != j && resFunc(c1, c2, matrix.matrix[i][0], matrix.matrix[j][0]) == f) {
                        x1 = matrix.matrix[i][0];
                        x2 = matrix.matrix[j][0];
                        break;
                    }
                }
            }
        }
    }

    private void findRow(Matrix matrix) {
        double min = Double.MAX_VALUE;
        for (int row = 0; row < matrix.getRows() - 1; row++) {
            if (matrix.getE(row, 0) / matrix.getE(row, indexOfLeadingCol) < min
                    && matrix.getE(row, 0) / matrix.getE(row, indexOfLeadingCol) >= 0) {
                min = matrix.getE(row, 0) / matrix.getE(row, indexOfLeadingCol);
                indexOfLeadingRow = row;
            }
        }
        leadingElem = matrix.getE(indexOfLeadingRow, indexOfLeadingCol);
    }

    private Matrix copy(Matrix matrix, Matrix modified) {
        Matrix resMatrix = new Matrix(ROW_COUNT - 1, COL_COUNT);
        for (int i = 0; i < matrix.getRows(); i++)
            resMatrix.matrix[i] = Arrays.copyOf(modified.matrix[i], modified.getCols());
        return resMatrix;
    }

    private void findCol(double[] row) {
        double min = Double.MAX_VALUE;
        for (int i = 0; i < row.length; i++) {
            if (row[i] == 0) continue;
            else if (row[i] < min) {
                min = row[i];
                indexOfLeadingCol = i;
            }
        }
    }

    public class Matrix {

        @Getter
        double[][] matrix;
        @Getter
        private int rows;
        @Getter
        private int cols;

        Matrix(int rows, int cols) {
            this.rows = rows + 1;
            this.cols = cols + rows;
            matrix = new double[this.rows][this.cols];
            for (int i = 0; i < rows + 1; i++)
                for (int j = cols - 1; j < this.cols; j++)
                    if (i == j - cols) matrix[i][j] = 1;
                    else if (i == rows) matrix[i][j] = 0;
                    else matrix[i][j] = 0;
        }

        public void setRow(int index, double[] row) {
            for (int i = 0; i < row.length; i++) {
                matrix[index][i] = row[i];
            }
        }

        public double getE(int i, int j) {
            return matrix[i][j];
        }

        public double[] getRow(int i) {
            return matrix[i];
        }

        public double[] getFunc() {
            return matrix[rows - 1];
        }
    }


}
