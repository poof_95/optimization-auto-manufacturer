package com.abataikin.opr.gui;

import com.vaadin.annotations.Title;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import java.net.MalformedURLException;
import java.net.URI;

@SpringUI(path = "/login")
@Title("Авторизация")
public class LoginUI extends UI {

    @Autowired
    private DaoAuthenticationProvider daoAuthenticationProvider;

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        if (!(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)) {
            URI currentLoc = Page.getCurrent().getLocation();
            try {
                Page.getCurrent().setLocation(currentLoc.toURL().toString().replace("/login", "/main"));
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            }
            return;
        }

        VerticalLayout vl = new VerticalLayout();
        LoginForm lf = new LoginForm();
        lf.setLoginButtonCaption("Войти");
        lf.setPasswordCaption("Пароль:");
        lf.setUsernameCaption("Логин:");

        lf.addLoginListener(e -> {
            try {
                Authentication auth = new UsernamePasswordAuthenticationToken(e.getLoginParameter("username"), e.getLoginParameter("password"));
                Authentication authenticated = daoAuthenticationProvider.authenticate(auth);
                SecurityContextHolder.getContext().setAuthentication(authenticated);
                getPage().setLocation("/main");
            } catch (final AuthenticationException ex) {
                String message = "Ошибка! Неверый логин или пароль!\nЛогин: "+ e.getLoginParameter("username") + "\nПароль: " + e.getLoginParameter("password");
                Notification.show(message, Notification.Type.ERROR_MESSAGE);
            }

        });

        vl.addComponent(lf);
        vl.setComponentAlignment(lf, Alignment.MIDDLE_CENTER);
        vl.setSizeFull();
        setContent(vl);
    }
}
