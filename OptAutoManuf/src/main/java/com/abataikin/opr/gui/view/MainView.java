package com.abataikin.opr.gui.view;

import com.abataikin.opr.gui.forms.GeneticForm;
import com.abataikin.opr.gui.forms.SimplexForm;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.*;

@SpringComponent
@UIScope
public class MainView extends VerticalLayout implements View {

    private final SimplexForm simplexForm;
    private final GeneticForm geneticForm;

    private Panel panel;
    private Accordion accordion;

    private TextField c1;
    private TextField c2;

    public MainView(SimplexForm simplexForm, GeneticForm geneticForm) {
        this.simplexForm = simplexForm;
        this.geneticForm = geneticForm;

        setSizeFull();

        VerticalLayout conten = new VerticalLayout();
        HorizontalLayout layout = new HorizontalLayout();
        Label oper = new Label(" + ");
        Label expr = new Label( " -> MAX");
        c1 = new TextField();
        c2 = new TextField();
        panel = new Panel("Целевая функция:");
        accordion = new Accordion();

        panel.setIcon(VaadinIcons.NOTEBOOK);

        accordion.setSizeFull();
        this.simplexForm.setSizeFull();
        this.geneticForm.setSizeFull();

        c1.setPlaceholder("C1");
        c2.setPlaceholder("C2");
        c1.setValue("7");
        c2.setValue("5");

        this.simplexForm.setC1(c1);
        this.simplexForm.setC2(c2);
        this.geneticForm.setC1(c1);
        this.geneticForm.setC2(c2);

        accordion.addTab(this.simplexForm, "Симплекс-метод", VaadinIcons.FUNCION);
        accordion.addTab(this.geneticForm, "Гинетический алгоритм", VaadinIcons.FUNCION);
        layout.addComponents(c1, oper, c2, expr);
        conten.addComponent(layout);

        accordion.setCaption("Выберите метод решения:");
        accordion.setIcon(VaadinIcons.MENU);

        layout.setComponentAlignment(oper, Alignment.MIDDLE_CENTER);
        layout.setComponentAlignment(expr, Alignment.MIDDLE_CENTER);
        conten.setComponentAlignment(layout,Alignment.MIDDLE_CENTER);

        panel.setContent(conten);
        this.addComponents(panel, accordion);
    }

}
