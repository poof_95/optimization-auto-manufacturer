package com.abataikin.opr.gui.windows;

import com.abataikin.opr.core.SimplexMethod;
import com.vaadin.data.HasValue;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.*;
import lombok.Getter;

import java.util.ArrayList;

public class ResultSimplexWindow extends Window implements Button.ClickListener, HasValue.ValueChangeListener {

    private final SimplexMethod simplexMethod;
    private final String TITLE = "Результаты";

    private VerticalLayout contetn;
    private Button btnOk;
    private ComboBox<String> iterations;
    private Grid<SimplexMatrixRowToList> grid;
    private ArrayList<String> getTitleCombobox;
    private ArrayList<ArrayList<SimplexMatrixRowToList>> listForGrid;

    public ResultSimplexWindow(SimplexMethod simplexMethod) {
        this.simplexMethod = simplexMethod;

        setCaption(TITLE);
        setResizable(false);
        setDraggable(false);
        setModal(true);
        setWidth(45,Unit.PERCENTAGE);
        center();
        setIcon(VaadinIcons.NOTEBOOK);

        String x1 = String.format("%8.2f", simplexMethod.getX1()).replace(",", ".");
        String x2 = String.format("%8.2f", simplexMethod.getX2()).replace(",", ".");
        String f = String.format("%8.2f", simplexMethod.getF()).replace(",", ".");

        Label x1Label = new Label("Автомашины марки А1: " + x1 + " шт.");
        Label x2Label = new Label("Автомашины марки А2: " + x2 + " шт.");
        Label fLabel = new Label("Выручка предприятия: " + f + " тыс. руб.");

        HorizontalLayout labelLayout = new HorizontalLayout();
        contetn = new VerticalLayout();
        iterations = new ComboBox<>("Итерации:");
        listForGrid = new ArrayList<>();
        grid = new Grid<>();

        iterations.setEmptySelectionAllowed(false);
        iterations.setPlaceholder("Не выбрана итерация");
        iterations.setWidth(30,Unit.PERCENTAGE);

        getTitleCombobox = new ArrayList<>();
        for (int i = 0; i < simplexMethod.getList().size(); i++){
            getTitleCombobox.add((i + 1) + "-ая итерация");
            ArrayList<SimplexMatrixRowToList> listRows = new ArrayList<>();
            for (int j = 0; j < simplexMethod.getList().get(i).getRows();j++){
                listRows.add(new SimplexMatrixRowToList(simplexMethod.getList().get(i).getRow(j)));
            }
            listForGrid.add(listRows);
        }

        iterations.setItems(getTitleCombobox);
        iterations.addValueChangeListener(this);

        grid.addColumn(SimplexMatrixRowToList::getZ).setCaption("Ограничения");
        grid.addColumn(SimplexMatrixRowToList::getX1).setCaption("X1");
        grid.addColumn(SimplexMatrixRowToList::getX2).setCaption("X2");
        grid.addColumn(SimplexMatrixRowToList::getS1).setCaption("S1");
        grid.addColumn(SimplexMatrixRowToList::getS2).setCaption("S2");
        grid.addColumn(SimplexMatrixRowToList::getS3).setCaption("S3");
        grid.addColumn(SimplexMatrixRowToList::getS4).setCaption("S4");

        grid.setSizeFull();
        grid.setVisible(false);

        btnOk = new Button("OK");

        btnOk.addClickListener(this);
        btnOk.setSizeFull();

        labelLayout.addComponents(x1Label, x2Label, fLabel, btnOk);

        contetn.addComponents(labelLayout, iterations, grid, btnOk);
        contetn.setComponentAlignment(labelLayout, Alignment.MIDDLE_CENTER);
        contetn.setComponentAlignment(btnOk, Alignment.MIDDLE_CENTER);

        setContent(contetn);
    }

    @Override
    public void buttonClick(Button.ClickEvent clickEvent) {
        Object obj = clickEvent.getSource();
        if (obj == btnOk) {
            close();
        } else
            throw new RuntimeException("Unknown source: " + clickEvent.getSource());

    }

    @Override
    public void valueChange(HasValue.ValueChangeEvent valueChangeEvent) {
        if (!grid.isVisible()) {
            grid.setVisible(true);
            center();
        }
        for (int i = 0; i < getTitleCombobox.size(); i++){
            if (valueChangeEvent.getValue().toString().contains(String.valueOf(i + 1))){
                grid.setItems(listForGrid.get(i));
                break;
            }
        }
    }

    class SimplexMatrixRowToList {
        @Getter
        private double z;
        @Getter
        private double x1;
        @Getter
        private double x2;
        @Getter
        private double s1;
        @Getter
        private double s2;
        @Getter
        private double s3;
        @Getter
        private double s4;

        SimplexMatrixRowToList(double[] row){
            z = row[0];
            x1 = row[1];
            x2 = row[2];
            s1 = row[3];
            s2 = row[4];
            s3 = row[5];
            s4 = row[6];
        }

    }

}
