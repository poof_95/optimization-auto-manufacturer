package com.abataikin.opr.gui.forms;

import com.abataikin.opr.core.SimplexMethod;
import com.abataikin.opr.gui.windows.ResultSimplexWindow;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.*;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

public class SimplexForm extends VerticalLayout implements Button.ClickListener {

    private final SimplexMethod simplexMethod;

    @Setter
    private TextField c1;
    @Setter
    private TextField c2;

    private Button btnSolve;
    private Button btnClean;
    private Button btnDefault;
    private ResultSimplexWindow resultWindow;
    private ArrayList<LimitStamp> list;

    public SimplexForm(SimplexMethod simplexMethod) {
        this.simplexMethod = simplexMethod;

        list = new ArrayList<>();

        btnSolve = new Button("Рассчитать", VaadinIcons.START_COG);
        btnClean = new Button("Очистить", VaadinIcons.ERASER);
        btnDefault = new Button("Установить значения по заданию", VaadinIcons.ROTATE_LEFT);

        list.add(new LimitStamp(true));
        list.add(new LimitStamp(true));
        list.add(new LimitStamp(false));
        list.add(new LimitStamp(false));

        list.get(3).getX1Label().setValue("x2");

        btnSolve.addClickListener(this);
        btnClean.addClickListener(this);
        btnDefault.addClickListener(this);

        btnSolve.setWidth(34.8f, Unit.PERCENTAGE);
        btnClean.setWidth(34.8f, Unit.PERCENTAGE);
        btnDefault.setWidth(34.8f, Unit.PERCENTAGE);

        for (LimitStamp l : list) {
            addComponent(l);
            setComponentAlignment(l, Alignment.MIDDLE_CENTER);
        }

        addComponents(btnSolve, btnClean, btnDefault);
        setComponentAlignment(btnSolve, Alignment.BOTTOM_CENTER);
        setComponentAlignment(btnClean, Alignment.BOTTOM_CENTER);
        setComponentAlignment(btnDefault, Alignment.BOTTOM_CENTER);

    }

    @Override
    public void buttonClick(Button.ClickEvent clickEvent) {
        Object obj = clickEvent.getSource();
        if (obj == btnSolve) {
            double[][] matr;
            try {
                double doubC1 = Double.parseDouble(c1.getValue());
                double doubC2 = Double.parseDouble(c2.getValue());
                matr = new double[][]{
                        {Double.parseDouble(list.get(0).getTfLimit().getValue()), Double.parseDouble(list.get(0).getTfX1().getValue()), Double.parseDouble(list.get(0).getTfX2().getValue())},
                        {Double.parseDouble(list.get(1).getTfLimit().getValue()), Double.parseDouble(list.get(1).getTfX1().getValue()), Double.parseDouble(list.get(1).getTfX2().getValue())},
                        {Double.parseDouble(list.get(2).getTfLimit().getValue()), 1.0, 0.0},
                        {Double.parseDouble(list.get(3).getTfLimit().getValue()), 0.0, 1.0},
                        {0.0, -doubC1, -doubC2}
                };
                
                simplexMethod.setC1(doubC1);
                simplexMethod.setC2(doubC2);
                simplexMethod.setMatr(matr);
                simplexMethod.solve();
                resultWindow = new ResultSimplexWindow(simplexMethod);
                getUI().addWindow(resultWindow);
            } catch (IllegalArgumentException e) {
                Notification.show("Ошибка!", "Некорректные данные", Notification.Type.ERROR_MESSAGE);
            }
        } else if (obj == btnClean) {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).getTfLimit().clear();
                list.get(i).getTfX1().clear();
                if (i < 2)
                    list.get(i).getTfX2().clear();
            }
        } else if (obj == btnDefault) {
            list.get(0).getTfX1().setValue("2");
            list.get(0).getTfX2().setValue("3");
            list.get(0).getTfLimit().setValue("17");
            list.get(1).getTfX1().setValue("2");
            list.get(1).getTfX2().setValue("1");
            list.get(1).getTfLimit().setValue("11");
            list.get(2).getTfLimit().setValue("6");
            list.get(3).getTfLimit().setValue("5");

        } else
            throw new RuntimeException("Unknown source: " + obj);
    }

    class LimitStamp extends HorizontalLayout {

        private Label plusLabel;
        private Label operLabel;
        @Getter
        private Label x1Label;
        @Getter
        private TextField tfX1;
        @Getter
        private TextField tfX2;
        @Getter
        private TextField tfLimit;

        LimitStamp(boolean flag) {
            plusLabel = new Label("+");
            operLabel = new Label("<=");
            x1Label = new Label("x1");
            tfX1 = new TextField();
            tfX2 = new TextField();
            tfLimit = new TextField();

            tfX1.setPlaceholder("x1");
            tfX2.setPlaceholder("x2");
            tfLimit.setPlaceholder("Ограничение");

            if (!flag) {
                addComponents(x1Label, operLabel, tfLimit);
                setComponentAlignment(operLabel, Alignment.MIDDLE_CENTER);
                setComponentAlignment(x1Label, Alignment.MIDDLE_CENTER);
            } else {
                addComponents(tfX1, plusLabel, tfX2, operLabel, tfLimit);
                setComponentAlignment(operLabel, Alignment.MIDDLE_CENTER);
                setComponentAlignment(plusLabel, Alignment.MIDDLE_CENTER);
            }
        }
    }
}
