package com.abataikin.opr.gui;

import com.abataikin.opr.gui.view.InfoView;
import com.abataikin.opr.gui.view.MainView;
import com.vaadin.annotations.Title;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

@SpringUI(path = "/main")
@Title("Оптимизация автопроизводителя")
public class OptUI extends UI implements MenuBar.Command {

    private final MainView myForm;
    private final String MAIN_VIEW = "mainView";
    private final String INFO_VIEW = "infoView";
    private final VerticalLayout layout = new VerticalLayout();
    private final VerticalLayout content = new VerticalLayout();
    private final MenuBar menuBar = new MenuBar();

    private Navigator navigator;
    private MenuBar.Command cmdMain;
    private MenuBar.Command cmdInfo;
    private MenuBar.Command cmdExit;
    private MenuBar.MenuItem mainItem;
    private MenuBar.MenuItem infoItem;
    private MenuBar.MenuItem exitItem;

    public OptUI(MainView myForm) {
        this.myForm = myForm;
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setContent(layout);
        navigator = new Navigator(this, content);
        navigator.addView(MAIN_VIEW, this.myForm);
        navigator.addView(INFO_VIEW, new InfoView());

        navigator.navigateTo(MAIN_VIEW);

        cmdMain = this::menuSelected;
        cmdInfo = this::menuSelected;
        cmdExit = this::menuSelected;

        mainItem = menuBar.addItem("Главная", VaadinIcons.HOME, cmdMain);
        infoItem = menuBar.addItem("О проекте...", VaadinIcons.INFO_CIRCLE, cmdInfo);
        exitItem = menuBar.addItem("Выход", VaadinIcons.SIGN_OUT, cmdExit);

        menuBar.setStyleName(ValoTheme.MENUBAR_BORDERLESS);
        layout.addComponents(menuBar, content);
        content.setMargin(false);
        layout.setMargin(false);
    }

    @Override
    public void menuSelected(MenuBar.MenuItem menuItem) {
        if (menuItem == this.mainItem) {
            navigator.navigateTo(MAIN_VIEW);
        } else if (menuItem == this.infoItem) {
            navigator.navigateTo(INFO_VIEW);
        } else if (menuItem == exitItem) {
            VaadinService.getCurrentRequest().getWrappedSession().invalidate();
            new SecurityContextLogoutHandler()
                    .logout(((com.vaadin.server.VaadinServletRequest) VaadinService.getCurrentRequest())
                            .getHttpServletRequest(), null, null);
            Page.getCurrent().setLocation("/login");
        } else
            throw new RuntimeException("Unknown menuItem: " + menuItem.getText());
    }
}
