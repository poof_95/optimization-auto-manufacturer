package com.abataikin.opr.gui.view;

import com.vaadin.navigator.View;
import com.vaadin.server.*;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;

public class InfoView extends VerticalLayout implements View {

    private final String TASK_TEXT = "Предприятие производит сборку автомашин двух марок: A1 и A2 . Для этого требуются " +
            "следующие материалы: S1 – комплекты заготовок металлоконструкций в количестве b1 = 17 шт., необходимые для " +
            "сборки автомашин марок A1 и A2 (соответственно 2 и 3 ед.); S2 – комплекты  резиновых изделий  в количестве " +
            "b2 = 11 шт. (соответственно 2 и 1 ед.); S3 – двигатели с арматурой и электрооборудованием в количестве b3 =" +
            " 6 комплектов, необходимых по одному для каждой автомашины марки A1 ; S4  – двигатели  с  арматурой и  " +
            "электрооборудованием в  количестве b4 = 5 комплектов, необходимых по одному для каждой автомашины марки A2." +
            " Стоимость автомашины марки A1 равна c1 = 7 тыс. ден. ед., а стоимость марки автомашины A2 – c2 = 5 тыс. " +
            "ден. ед. Определить план выпуска, составляющий максимальную выручку.";
    private final String AUTOR = "Курсовой проект выполнисл студент группы ИСТ-41 " +
            "Батайкин Алексей Сергеевич <br> <center> &copy; 2018 г., УО БарГУ, Инженерный факультет.";

    private String basepath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
    private TextArea ta;
    private Label copyrait;

    public InfoView() {

        copyrait = new Label(AUTOR, ContentMode.HTML);
        ta = new TextArea("Задание:");
        ta.setWordWrap(true);
        ta.setValue(TASK_TEXT);
        ta.setReadOnly(true);
        ta.setSizeFull();

        Image funcImg = new Image("Целевая функция: ", new ClassResource("/images/func.png"));
        Image limitsImg = new Image("Ограничения:", new ClassResource("/images/limits.png"));
        Image excelImg = new Image("Excel:", new ClassResource("/images/excel.png"));

        funcImg.setWidth(15, Unit.PERCENTAGE);
        funcImg.setHeight(15, Unit.PERCENTAGE);
        limitsImg.setWidth(10, Unit.PERCENTAGE);
        limitsImg.setHeight(10, Unit.PERCENTAGE);

        addComponents(ta, funcImg, limitsImg, excelImg, copyrait);
        setComponentAlignment(funcImg, Alignment.TOP_CENTER);
        setComponentAlignment(limitsImg, Alignment.TOP_CENTER);
        setComponentAlignment(excelImg, Alignment.TOP_CENTER);
        setComponentAlignment(copyrait, Alignment.TOP_CENTER);

    }
}
