package com.abataikin.opr.gui.forms;

import com.abataikin.opr.core.GeneticAlgorithm;
import com.abataikin.opr.gui.windows.ResultGeneticWindow;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.*;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

public class GeneticForm extends VerticalLayout implements Button.ClickListener {

    private final GeneticAlgorithm geneticAlgorithm;

    @Setter
    private TextField c1;
    @Setter
    private TextField c2;

    private Button btnSolve;
    private Button btnClean;
    private Button btnDefault;
    private ArrayList<LimitStamp> list;
    private ResultGeneticWindow geneticWindow;

    private TextField tfNumberOfGen;
    private TextField tfNumberOfChild;
    private TextField tfNumberOfParents;

    public GeneticForm(GeneticAlgorithm geneticAlgorithm) {
        this.geneticAlgorithm = geneticAlgorithm;

        list = new ArrayList<>();
        btnSolve = new Button("Рассчитать", VaadinIcons.START_COG);
        btnClean = new Button("Очистить", VaadinIcons.ERASER);
        btnDefault = new Button("Установить значения по заданию", VaadinIcons.ROTATE_LEFT);
        tfNumberOfGen = new TextField();
        tfNumberOfChild = new TextField();
        tfNumberOfParents = new TextField();

        tfNumberOfGen.setPlaceholder("Количество поколений");
        tfNumberOfChild.setPlaceholder("Количество потомков");
        tfNumberOfParents.setPlaceholder("Количество родителей");

        list.add(new LimitStamp(true));
        list.add(new LimitStamp(true));
        list.add(new LimitStamp(false));
        list.add(new LimitStamp(false));

        list.get(3).getX1Label().setValue("x2");

        btnSolve.addClickListener(this);
        btnClean.addClickListener(this);
        btnDefault.addClickListener(this);

        btnSolve.setWidth(34.8f, Unit.PERCENTAGE);
        btnClean.setWidth(34.8f, Unit.PERCENTAGE);
        btnDefault.setWidth(34.8f, Unit.PERCENTAGE);
        tfNumberOfGen.setWidth(34.8f, Unit.PERCENTAGE);
        tfNumberOfChild.setWidth(34.8f, Unit.PERCENTAGE);
        tfNumberOfParents.setWidth(34.8f, Unit.PERCENTAGE);

        for (LimitStamp l : list) {
            addComponent(l);
            setComponentAlignment(l, Alignment.MIDDLE_CENTER);
        }

        addComponents(tfNumberOfGen, tfNumberOfChild, tfNumberOfParents, btnSolve, btnClean, btnDefault);

        setComponentAlignment(tfNumberOfGen, Alignment.MIDDLE_CENTER);
        setComponentAlignment(tfNumberOfChild, Alignment.MIDDLE_CENTER);
        setComponentAlignment(tfNumberOfParents, Alignment.MIDDLE_CENTER);
        setComponentAlignment(btnSolve, Alignment.BOTTOM_CENTER);
        setComponentAlignment(btnClean, Alignment.BOTTOM_CENTER);
        setComponentAlignment(btnDefault, Alignment.BOTTOM_CENTER);
    }

    @Override
    public void buttonClick(Button.ClickEvent clickEvent) {
        Object obj = clickEvent.getSource();
        if (obj == btnSolve) {
            double[] limits;
            double[][] values;
            double numbGen;
            double numbChild;
            double numbParents;
            try {
                double doubC1 = Double.parseDouble(c1.getValue());
                double doubC2 = Double.parseDouble(c2.getValue());
                limits = new double[]{
                        Double.parseDouble(list.get(0).getTfLimit().getValue()),
                        Double.parseDouble(list.get(1).getTfLimit().getValue()),
                        Double.parseDouble(list.get(2).getTfLimit().getValue()),
                        Double.parseDouble(list.get(3).getTfLimit().getValue())
                };
                values = new double[][]{
                        new double[]{Double.parseDouble(list.get(0).getTfX1().getValue()), Double.parseDouble(list.get(0).getTfX2().getValue())},
                        new double[]{Double.parseDouble(list.get(1).getTfX1().getValue()), Double.parseDouble(list.get(1).getTfX2().getValue())}
                };
                numbGen = Double.parseDouble(tfNumberOfGen.getValue());
                numbChild = Double.parseDouble(tfNumberOfChild.getValue());
                numbParents = Double.parseDouble(tfNumberOfParents.getValue());

                geneticAlgorithm.setC1(doubC1);
                geneticAlgorithm.setC2(doubC2);
                geneticAlgorithm.setLimits(limits);
                geneticAlgorithm.setValues(values);
                geneticAlgorithm.setNumberOfGen(numbGen);
                geneticAlgorithm.setNumberOfChild(numbChild);
                geneticAlgorithm.setNumberOfParents(numbParents);
                geneticAlgorithm.solve();

                geneticWindow = new ResultGeneticWindow(geneticAlgorithm);
                getUI().addWindow(geneticWindow);

            } catch (IllegalArgumentException e) {
                Notification.show("Ошибка!", "Некорректные данные", Notification.Type.ERROR_MESSAGE);
            }

        } else if (obj == btnClean) {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).getTfLimit().clear();
                list.get(i).getTfX1().clear();
                if (i < 2)
                    list.get(i).getTfX2().clear();
            }
            tfNumberOfGen.clear();
            tfNumberOfChild.clear();
            tfNumberOfParents.clear();
        } else if (obj == btnDefault) {
            list.get(0).getTfX1().setValue("2");
            list.get(0).getTfX2().setValue("3");
            list.get(0).getTfLimit().setValue("17");
            list.get(1).getTfX1().setValue("2");
            list.get(1).getTfX2().setValue("1");
            list.get(1).getTfLimit().setValue("11");
            list.get(2).getTfLimit().setValue("6");
            list.get(3).getTfLimit().setValue("5");
            tfNumberOfGen.setValue("5");
            tfNumberOfChild.setValue("100");
            tfNumberOfParents.setValue("20");
        } else
            throw new RuntimeException("Unknown source: " + obj);
    }

    class LimitStamp extends HorizontalLayout {

        private Label plusLabel;
        private Label operLabel;
        @Getter
        private Label x1Label;
        @Getter
        private TextField tfX1;
        @Getter
        private TextField tfX2;
        @Getter
        private TextField tfLimit;

        LimitStamp(boolean flag) {
            plusLabel = new Label("+");
            operLabel = new Label("<=");
            x1Label = new Label("x1");
            tfX1 = new TextField();
            tfX2 = new TextField();
            tfLimit = new TextField();

            tfX1.setPlaceholder("x1");
            tfX2.setPlaceholder("x2");
            tfLimit.setPlaceholder("Ограничение");

            if (!flag) {
                addComponents(x1Label, operLabel, tfLimit);
                setComponentAlignment(operLabel, Alignment.MIDDLE_CENTER);
                setComponentAlignment(x1Label, Alignment.MIDDLE_CENTER);
            } else {
                addComponents(tfX1, plusLabel, tfX2, operLabel, tfLimit);
                setComponentAlignment(operLabel, Alignment.MIDDLE_CENTER);
                setComponentAlignment(plusLabel, Alignment.MIDDLE_CENTER);
            }
        }
    }
}
