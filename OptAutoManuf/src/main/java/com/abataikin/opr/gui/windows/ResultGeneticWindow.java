package com.abataikin.opr.gui.windows;

import com.abataikin.opr.core.GeneticAlgorithm;
import com.vaadin.data.HasValue;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.*;

import java.util.ArrayList;

public class ResultGeneticWindow extends Window implements Button.ClickListener, HasValue.ValueChangeListener {

    private final GeneticAlgorithm geneticAlgorithm;
    private final String TITLE = "Результаты";

    private VerticalLayout contetn;
    private Button btnOk;
    private ComboBox<String> generations;
    private Grid<GeneticAlgorithm.Point> grid;
    private ArrayList<String> getTitleCombobox;

    public ResultGeneticWindow(GeneticAlgorithm geneticAlgorithm) {
        this.geneticAlgorithm = geneticAlgorithm;

        setCaption(TITLE);
        setResizable(false);
        setDraggable(false);
        setModal(true);
        setWidth(45,Unit.PERCENTAGE);
        center();
        setIcon(VaadinIcons.NOTEBOOK);

        Label x1Label = new Label("Автомашины марки А1: " + geneticAlgorithm.getX1() + " шт.");
        Label x2Label = new Label("Автомашины марки А2: " + geneticAlgorithm.getX2() + " шт.");
        Label fLabel = new Label("Выручка предприятия: " + geneticAlgorithm.getF() + " тыс. руб.");
        HorizontalLayout labelLayout = new HorizontalLayout();
        generations = new ComboBox<>("Поколение:");
        grid = new Grid<>();

        generations.setEmptySelectionAllowed(false);
        generations.setPlaceholder("Не выбрано поколение");
        generations.setWidth(30,Unit.PERCENTAGE);

        getTitleCombobox = new ArrayList<>();
        for (int i = 0; i < geneticAlgorithm.getGenerations().size(); i++){
            getTitleCombobox.add((i + 1) + "-ое поколение");
        }

        generations.setItems(getTitleCombobox);
        generations.addValueChangeListener(this);

        grid.addColumn(GeneticAlgorithm.Point::getX1).setCaption("Автомашины марки А1");
        grid.addColumn(GeneticAlgorithm.Point::getX2).setCaption("Автомашины марки А2");
        grid.addColumn(GeneticAlgorithm.Point::getF).setCaption("Выручка предприятия");
        grid.setSizeFull();
        grid.setVisible(false);

        labelLayout.addComponents(x1Label,x2Label,fLabel);

        contetn = new VerticalLayout();
        btnOk = new Button("OK");

        btnOk.addClickListener(this);
        btnOk.setSizeFull();

        contetn.addComponents(labelLayout,generations,grid,btnOk);
        contetn.setComponentAlignment(labelLayout, Alignment.MIDDLE_CENTER);
        contetn.setComponentAlignment(btnOk, Alignment.MIDDLE_CENTER);

        setContent(contetn);

    }

    @Override
    public void buttonClick(Button.ClickEvent clickEvent) {
        Object obj = clickEvent.getSource();
        if (obj == btnOk){
            close();
        }else
            throw new RuntimeException("Unknown source: " + clickEvent.getSource());
    }

    @Override
    public void valueChange(HasValue.ValueChangeEvent valueChangeEvent) {
        if (!grid.isVisible()) {
            grid.setVisible(true);
            center();
        }
        for (int i = 0; i < getTitleCombobox.size(); i++){
            if (valueChangeEvent.getValue().toString().contains(String.valueOf(i + 1))){
                grid.setItems(geneticAlgorithm.getGenerations().get(i));
                break;
            }
        }
    }
}
